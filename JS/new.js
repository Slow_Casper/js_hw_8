// TASK 1

let pColor = document.querySelectorAll("p");

for (const elem of pColor) {
    elem.style.color = "#ff0000";
};

// TASK 2

let options = document.getElementById("optionsList");
let parent = options.parentElement;
let childs = options.children;

console.log(options);
console.log(parent);
console.log(childs);

// TASK 3

let test = document.querySelector("#testParagraph");
test.textContent = "This is a paragraph";

console.log(test);

// TASK 4

let main = document.querySelector(".main-header").children;

console.log(main);

for (const el of main) {
    el.classList += " nav-item"
};

// TASK 5

let sectionEl = document.querySelectorAll(".section-title");

for (const elem of sectionEl) {
    if(elem.className.includes("section-title")){
        elem.classList.remove("section-title")
    };
};

console.log(sectionEl);