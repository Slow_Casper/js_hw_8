1. Опишіть своїми словами що таке Document Object Model (DOM)

    Document Object Model, або DOM, це дерево документа, яке містить у собі вузли (теги та текстові вузли) для роботи. Тобто - DOM містить у собі весь вміст HTML- документу (включаючи коментарі).

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerHTML - виводить всю структуру дочірніх елементів (а також їх дочірніх елементів) для даного елемента (не включаючи, власне, сам елемент).

    innerText - виводить тільки текст, який знаходиться у всіх вкладених елементах (включаючи сам елемент)

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

    Звернутись до об'єкта можна за допомогою пошуку по класу елемента, ID - елемента, властивості name та за назвою тега.

    document.getElementByID

    document.getElementsByClassName
    document.getElementsByName
    document.getElementsByTagName

    Найчастіше використовується document.querySelector(CSS selector) та document.querySelectorAll(CSS selector). 

    Перший варіант шукає перший елемент з цим селектором. Другий варіант шукає всі елементи з цим селектором